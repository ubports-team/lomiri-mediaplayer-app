lomiri-mediaplayer-app (1.1.0+dfsg-3) unstable; urgency=medium

  * debian/control:
    + Add to B-D:/D: libmpris-qt5-dev (build-time) and qml-module-org-
      nemomobile-mpris (runtime).
  * debian/patches:
    + Add 1001_embedded-mpris-support.patch. Provide embedded MPRIS support,
      so mediaplayer app can dock its controls into the sound indicator
      without the need of media-hub being installed on the system.
  * debian/rules:
    + Build with embedded MPRIS support enabled.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 02 Dec 2024 15:00:21 +0100

lomiri-mediaplayer-app (1.1.0+dfsg-2) unstable; urgency=medium

  * debian/:
    + Enable user-session-migration debhelper feature.
    + Add user-session-migration script lomiri-mediaplayer-app_01_update-
      content-hub.sh.
  * debian/control:
    + In B-D, switch from pkg-config to pkgconf.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 13 Sep 2024 10:11:07 +0200

lomiri-mediaplayer-app (1.1.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Rebase 2001_skip-autopilot-tests.patch.
  * debian/:
    + Adjust to new upstream source URL.
  * debian/lomiri-mediaplayer-app-common.install:
    + Adjust to namespace renamings.
  * debian/rules:
    + Amend path of CMakeLists.txt file removal.
  * debian/control:
    + Bump Standards-Version to 4.7.0. No changes needed.
  * debian/watch:
    + Update file for recent GitLab changes of the tags overview page.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 12 Sep 2024 12:06:58 +0200

lomiri-mediaplayer-app (1.0.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update copyright attribution for debian/.
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/source/lintian-overrides:
    + Drop very-long-line-length override for tests/videos/h264.avi.
      Lintian stopped complaining about that.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 05 Feb 2024 11:28:31 +0100

lomiri-mediaplayer-app (1.0.2+dfsg-1) unstable; urgency=medium

  * New upstream release.
    - Translation updates.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 27 Feb 2023 20:24:05 +0100

lomiri-mediaplayer-app (1.0.1+dfsg-1) unstable; urgency=medium

  * New upstream release. Upload to unstable.
  * debian/watch:
    + Start watching upstream releases.
  * debian/copyright:
    + Add upstream's debian/ folder to Files-Excluded.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 04 Feb 2023 22:31:57 +0100

lomiri-mediaplayer-app (1.0.0+git20230125.043a1ef+dfsg-1) experimental; urgency=medium

  * Initial upload to Debian. (Closes: #1030368).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 18 Jan 2023 13:05:30 +0100
